# About cachediffusion/logos

cachediffusion group develops MIT licensed open-source software from which hopefully everyone will benefit, in some way or another.

This repository contains all related logos for everyone to use as they deem fit, as they are licensed under the 
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/).

Please keep in mind that we ourselves act with only the best ideas and values in mind. See our CoC for more info about those.

# Contributing

Thank you for considering contributing to the project! The contribution guide can be found in
our [general documentation](https://codingdebt.de/docs/contributions).

# Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by
the [Code of Conduct](https://codingdebt.de/docs/contributions#code-of-conduct).

# Security Vulnerabilities

If you discover a security vulnerability within this software, please send an e-mail to us via [bytes@codingdebt.de](mailto:bytes@codingdebt.de).
All security vulnerabilities will be promptly addressed.

# License

This is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
