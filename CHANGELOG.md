# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.2 (2022-04-18)


### Bug Fixes

* **basar:** replaced placeholder logo with something defragmented ([f62c4fe](https://github.com/cachediffusion/logos/commit/f62c4feb4f5f3c000d3f610829633f730018e7a0))
* **namespace:** rename bazar to basar to make naming more clear ([e493108](https://github.com/cachediffusion/logos/commit/e493108e38a7bc488e215cf26c5c04837a8566b8))

### 0.0.1 (2022-04-18)


### Bug Fixes

* **namespace:** rename bazar to basar to make naming more clear ([e493108](https://github.com/cachediffusion/logos/commit/e493108e38a7bc488e215cf26c5c04837a8566b8))
